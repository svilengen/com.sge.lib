package com.sge.socket.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Enumeration;

public class Server {
    private int port;

    private static final int BUFFER_SIZE = 32;

    public Server(int port) {
        this.port = port;
    }

    public void start() throws IOException {
        ServerSocket ss = new ServerSocket(port);
        System.out.println("Server started at " + ss.toString());

        while (true) {
            Socket cs = ss.accept();
            System.out.println("Accepted connection from " + cs.toString());

            InputStream in = cs.getInputStream();
            OutputStream out = cs.getOutputStream();

            int recvMsgSize;
            // Size of received message
            byte[] receiveBuf = new byte[BUFFER_SIZE]; // Receive buffer

            // Receive until client closes connection, indicated by -1 return
            while ((recvMsgSize = in.read(receiveBuf)) != -1) {
                out.write(receiveBuf, 0, recvMsgSize);
            }
            cs.close();
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        Server server = new Server(1111);
        try {
            server.start();
        }
        catch (IOException e) {
            System.out.println("Server Error!");
            e.printStackTrace(System.out);
        }

    }

}
