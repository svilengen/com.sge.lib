package com.sge.socket.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

public class Client {
    private final String host;

    private final int port;

    public Client(String address, int port) {
        this.host = address;
        this.port = port;
    }

    public void connect() throws UnknownHostException, IOException {
        Socket cs = new Socket(host, port);
        InputStream in = cs.getInputStream();
        OutputStream out = cs.getOutputStream();

        String word = "hello world";
        out.write(word.getBytes());
        out.flush();

        byte[] data = word.getBytes();

        // Receive the same string back from the server
        int totalBytesRcvd = 0; // Total bytes received so far
        int bytesRcvd;
        // Bytes received in last read
        while (totalBytesRcvd < data.length) {
            if ((bytesRcvd = in.read(data, totalBytesRcvd, data.length - totalBytesRcvd)) == -1)
                throw new SocketException("Connection closed prematurely");
            totalBytesRcvd += bytesRcvd;
        } // data array is full

        System.out.println("Received: " + new String(data));
        cs.close();

    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        Client client = new Client("localhost", 1111);
        try {
            client.connect();
        }
        catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
