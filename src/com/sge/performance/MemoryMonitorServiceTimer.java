package com.webmethods.caf.memorymonitor.timer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.softwareag.caf.shared.memorymonitor.IMemoryData;
import com.softwareag.caf.shared.memorymonitor.MemoryMonitorList;
import com.sge.performance.MemoryMonitorUtil;
import com.webmethods.caf.memorymonitor.Messages;
import com.webmethods.portal.PortalException;
import com.webmethods.portal.bizPolicy.BizException;
import com.webmethods.portal.bizPolicy.IContext;
import com.webmethods.portal.bizPolicy.command.system.Restart;
import com.webmethods.portal.bizPolicy.impl.ContextFactory;
import com.webmethods.rtl.util.Debug;

/**
 * Class that manages and wraps {@link MemoryMonitorService}. Each time the {@link Configuration} is changed a new
 * instance of this class needs to be created and stored into Servlet Context ( see
 * {@link MemoryMonitorServiceServletContextLauncher} ), and then started ({@link #start()}).
 * @author bgsge
 * 
 */
public class MemoryMonitorServiceTimer {
    
    private static final Logger logger = Logger.getLogger(MemoryMonitorServiceTimer.class);

    private Timer timer;

    private final Configuration config;

    public List<IMemoryData> getMemoryData() {
        return MemoryMonitorService.getMemoryData();
    }

    /**
     * Delay of execution of memory monitoring service. In ms.
     */
    private final static int TIMER_DELAY = 5/* ms */* 1000;

    public MemoryMonitorServiceTimer(Configuration configuration) {
        config = configuration;

    }

    public void start() {
        if (getConfiguration().isEnabled() && timer == null) {
            logger.info("Memory Monitor Service is starting..."); //$NON-NLS-1$

            timer = new Timer("MemoryMonitorTimer", true); //$NON-NLS-1$
            MemoryMonitorService memoryMonitorService = new MemoryMonitorService(getConfiguration());
            timer.scheduleAtFixedRate(memoryMonitorService, new Date(), TIMER_DELAY);

            logger.info("Memory Monitor Service has been started."); //$NON-NLS-1$
        }
    }

    public void cancel() {
        if (timer != null) {
            logger.info("Memory Monitor Service is stopping..."); //$NON-NLS-1$

            timer.cancel();
            timer = null;

            logger.info("Memory Monitor Service has been stopped."); //$NON-NLS-1$
        }
    }

    public Configuration getConfiguration() {
        return config;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "MemoryMonitorServiceTimer [config=" + config + "]"; //$NON-NLS-1$ //$NON-NLS-2$
    }

    /**
     * This TimerTask will be invoked recurrently by the enclosing timer. This is the place where the actual memory
     * monitoring and acting upon it happens.
     * @author bgsge
     * 
     */
    private static final class MemoryMonitorService extends TimerTask {

        private enum Threshold {
            NONE("NONE"), WARN("WARN"), ERROR("ERROR"), FATAL("FATAL"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

            private final String stringValue;

            private Threshold(String stringValue) {
                this.stringValue = stringValue;
            }

            @Override
            public String toString() {
                return this.stringValue;
            }

        }

        // TODO access needs to be threadsafe!
        private final Configuration config;

        private final List<MemoryChangedListener> listeners;

        private static List<IMemoryData> mmList = Collections.synchronizedList(new MemoryMonitorList());

        public static List<IMemoryData> getMemoryData() {
            return mmList;
        }

        public MemoryMonitorService(Configuration configuration) {
            config = configuration;
            listeners = new ArrayList<MemoryChangedListener>();
            listeners.add(new MemoryChangedListener() {

                public void handleEvent(MemoryChangeEvent event) {
                    // check if total available memory is more than the configured threshold
                    if (event.getCalculatedFreeMemory() < event.getConfig().getThresholdFatal()) {

                        logger.warn("Memory Monitor Alert: min available free memory threshold " //$NON-NLS-1$
                            + MemoryMonitorUtil.format(event.getConfig().getThresholdFatal()) + " reached."); //$NON-NLS-1$

                        if (event.getConfig().isRestartServer()) {

                            try {

                                String emailContent = MemoryMonitorUtil.getEmailContent(
                                    Messages.getString("MemoryMonitorUtil.emailTemplate.header.restart"), event); //$NON-NLS-1$
                                MemoryMonitorUtil.writeToFileRestartEmailNotification(emailContent);
                            }
                            catch (IOException e) {
                                Debug
                                    .warn(
                                        "Unable to create restart notification file. Notification will not be sent on server restart.", e); //$NON-NLS-1$
                            }

                            try {
                                MemoryMonitorUtil.dumpHeap();
                            }
                            catch (Exception e) {
                                logger.fatal("Unable to execute Heap dump.", e); //$NON-NLS-1$
                            }

                            try {
                                logger.warn("Restarting MWS... "); //$NON-NLS-1$

                                IContext context = ContextFactory.acquireContext(true);

                                Restart restartCommand = (Restart) MemoryMonitorUtil.getCommandByName(Restart.COMMAND_NAME,
                                    context);

                                restartCommand.restart(context);

                                cancel();
                            }
                            catch (BizException e) {
                                logger.fatal("Unable to retrieve or execute Restart command.", e); //$NON-NLS-1$
                            }
                            catch (PortalException e) {
                                logger.fatal("Unable to retrieve or execute Restart command.", e); //$NON-NLS-1$
                            }

                        }

                    }

                }

            });

            listeners.add(new MemoryChangedListener() {

                public void handleEvent(MemoryChangeEvent event) {
                    mmList.add(event);
                }
            });

            listeners.add(new MemoryChangedListener() {

                Threshold lastThreshold = Threshold.NONE;

                public void handleEvent(MemoryChangeEvent event) {

                    if (event.getCalculatedFreeMemory() > event.getConfig().getThresholdWarn()) {
                        if (lastThreshold != Threshold.NONE) {
                            logger.info("Free memory is now in acceptable range."); //$NON-NLS-1$
                            MemoryMonitorUtil.sendThresholdBreachNotificationEmail(lastThreshold.toString(),
                                Threshold.NONE.toString(), event);
                            lastThreshold = Threshold.NONE;
                        }
                    }
                    else if (event.getCalculatedFreeMemory() <= event.getConfig().getThresholdWarn()
                        && event.getCalculatedFreeMemory() > event.getConfig().getThresholdError()) {
                        if (lastThreshold != Threshold.WARN) {
                            logger.warn(Threshold.WARN + " threshold breached!"); //$NON-NLS-1$
                            MemoryMonitorUtil.sendThresholdBreachNotificationEmail(lastThreshold.toString(),
                                Threshold.WARN.toString(), event);
                            lastThreshold = Threshold.WARN;
                        }

                    }
                    else if (event.getCalculatedFreeMemory() <= event.getConfig().getThresholdError()
                        && event.getCalculatedFreeMemory() > event.getConfig().getThresholdFatal()) {
                        if (lastThreshold != Threshold.ERROR) {
                            logger.warn(Threshold.ERROR + " threshold breached!"); //$NON-NLS-1$
                            MemoryMonitorUtil.sendThresholdBreachNotificationEmail(lastThreshold.toString(),
                                Threshold.ERROR.toString(), event);
                            lastThreshold = Threshold.ERROR;
                        }

                    }

                }
            });
        }

        @Override
        public void run() {
            Runtime runtime = Runtime.getRuntime();
            long currTime = System.currentTimeMillis();
            long totalMemory = MemoryMonitorUtil.convertBytesToMBytesLong(runtime.totalMemory());
            long freeMemory = MemoryMonitorUtil.convertBytesToMBytesLong(runtime.freeMemory());
            long maxMemory = MemoryMonitorUtil.convertBytesToMBytesLong(runtime.maxMemory());

            MemoryChangeEvent memoryChangeEvent = new MemoryChangeEvent(currTime, totalMemory, maxMemory, freeMemory,
                config);
            
            logger.debug("JVM Memory Stats: " + memoryChangeEvent); //$NON-NLS-1$ 
            logger.debug("Memory Monitor configuration: " + memoryChangeEvent.getConfig()); //$NON-NLS-1$

            fireMemoryChangeEvent(memoryChangeEvent);

        }

        public void fireMemoryChangeEvent(MemoryChangeEvent memoryChangeEvent) {

            for (MemoryChangedListener listener : listeners) {
                try {
                    listener.handleEvent(memoryChangeEvent);
                }
                catch (Exception e) {
                    logger.fatal("Memory changed event listener <" + listener + "> is faulty.", e); //$NON-NLS-1$ //$NON-NLS-2$
                }
            }
        }
    }
}
