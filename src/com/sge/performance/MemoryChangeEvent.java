package com.sge.performance;

import java.util.Date;

/**
 * Used to capture snapshot of memory statistics data and propagate to subscribed listeners. Immutable.
 * @author bgsge
 * 
 */
public class MemoryChangeEvent {

    private final long timeStamp;

    private final long totalMemory;

    private final long maxMemory;

    private final long freeMemory;

    private final Configuration config;

    /**
     * 
     * @param timeStamp
     * @param totalMemory in MB.
     * @param maxMemory in MB.
     * @param freeMemory in MB.
     * @param configuration
     */
    public MemoryChangeEvent(long timeStamp, long totalMemory, long maxMemory, long freeMemory,
        Configuration configuration) {
        this.timeStamp = timeStamp;
        this.totalMemory = totalMemory;
        this.maxMemory = maxMemory;
        this.freeMemory = freeMemory;
        this.config = configuration;
    }

    /**
     * 
     * @return the totalMemory in MB.
     */
    public long getTotalMemory() {
        return totalMemory;
    }

    /**
     * 
     * @return Currently started Memory Monitor instance's Configuration
     */
    public Configuration getConfig() {
        return config;
    }

    /**
     * @return the timeStamp
     */
    public long getTimeStamp() {
        return timeStamp;
    }

    /**
     * @return time stamp as Date
     */
    public Date getDate() {
        return new Date(timeStamp);
    }

    /**
     * @return the maxMemory in MB.
     */
    public long getMaxMemory() {
        return maxMemory;
    }

    /**
     * @return the freeMemory in MB.
     */
    public long getFreeMemory() {
        return freeMemory;
    }

    /**
     * 
     * @return Available Free JVM Memory in MBs.
     */
    public long getCalculatedFreeMemory() {
        // return real free memory of the JVM process
        long calculatedFreeMemory = getMaxMemory() - getTotalMemory() + getFreeMemory();

        return calculatedFreeMemory;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "MemoryChangeEvent [totalMemory=" + MemoryMonitorUtil.format(totalMemory) + ", maxMemory=" //$NON-NLS-1$ //$NON-NLS-2$
            + MemoryMonitorUtil.format(maxMemory)
            + ", freeMemory=" + MemoryMonitorUtil.format(freeMemory) + ", calculatedFreeMemory=" + MemoryMonitorUtil.format(getCalculatedFreeMemory()) + "]"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    }

}
