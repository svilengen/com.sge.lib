/*
 * Copyright 2011 by Software AG
 *
 * Uhlandstrasse 12, D-64297 Darmstadt, GERMANY
 *
 * All rights reserved
 *
 * This software is the confidential and proprietary
 * information of Software AG ('Confidential Information').
 * You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license
 * agreement you entered into with Software AG or its distributors.
 */

package com.sge.performance;

import java.text.NumberFormat;
import java.util.Locale;

public class MemoryMonitorUtil {
    
    /**
     * Bytes of 1 Megabyte.
     */
    public final static long UNIT_MB = 1024L * 1024L; /* bytes */
    
    public static String convertBytesToMBytes(long bytes) {
        return String.valueOf(convertBytesToMBytesLong(bytes));
    }

    public static long convertBytesToMBytesLong(long bytes) {
        return (long) (bytes / UNIT_MB);
    }
    

    /**
     * Only for debugging purposes. Do NOT use to format data reachable by the client.
     * @param number
     * @return
     */
    public static String format(long number) {
        NumberFormat instance = NumberFormat.getInstance(Locale.US);
        return instance.format(number) + "MB"; //$NON-NLS-1$
    }

}
