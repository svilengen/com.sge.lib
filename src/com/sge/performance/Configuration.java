package com.sge.performance;


/**
 * Configuration for {@link MemoryMonitorServiceTimer}. Immutable therefore thread safe.
 * @author bgsge
 * 
 */
public final class Configuration {
    private final long thresholdWarn;

    private final long thresholdError;

    private final long thresholdFatal;

    private final boolean restartServer;

    private final boolean enabled;

    private final String email;

    /**
     * Configuration of memory monitoring service
     * @param thresholdWarn in MB.
     * @param thresholdError in MB.
     * @param thresholdFatal in MB.
     * @param enabled
     */
    public Configuration(long thresholdWarn, long thresholdError, long thresholdFatal, boolean restartServer,
        boolean enabled, String email) {
        this.thresholdWarn = thresholdWarn;
        this.thresholdError = thresholdError;
        this.thresholdFatal = thresholdFatal;
        this.restartServer = restartServer;
        this.enabled = enabled;
        this.email = email;

    }

    public long getThresholdWarn() {
        return thresholdWarn;
    }

    public long getThresholdError() {
        return thresholdError;
    }

    public long getThresholdFatal() {
        return thresholdFatal;
    }

    public boolean isRestartServer() {
        return restartServer;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public String getEmail() {
        return email;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (enabled ? 1231 : 1237);
        result = prime * result + (int) (thresholdError ^ (thresholdError >>> 32));
        result = prime * result + (int) (thresholdFatal ^ (thresholdFatal >>> 32));
        result = prime * result + (int) (thresholdWarn ^ (thresholdWarn >>> 32));
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Configuration)) {
            return false;
        }
        Configuration other = (Configuration) obj;
        if (enabled != other.enabled) {
            return false;
        }
        if (thresholdError != other.thresholdError) {
            return false;
        }
        if (thresholdFatal != other.thresholdFatal) {
            return false;
        }
        if (thresholdWarn != other.thresholdWarn) {
            return false;
        }
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Configuration [thresholdWarn=" + MemoryMonitorUtil.format(thresholdWarn) + ", thresholdError=" //$NON-NLS-1$ //$NON-NLS-2$
            + MemoryMonitorUtil.format(thresholdError)
            + ", thresholdFatal=" + MemoryMonitorUtil.format(thresholdFatal) + ", restartServer=" + restartServer //$NON-NLS-1$ //$NON-NLS-2$
            + ", enabled=" + enabled + ", email=" + email + "]"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    }
}