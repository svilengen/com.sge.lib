package com.sge.performance;

/**
 * Listeners of {@link MemoryChangeEvent}s need to implement this interface
 * and subscribe with {@link MemoryMonitorServiceTimer}.
 * @author bgsge
 *
 */
public interface MemoryChangedListener {
    
    /**
     * Invoked by the memory monitor thread in recurring fashion. 
     * @param event
     */
    void handleEvent(MemoryChangeEvent event);

}
