package com.sge.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class ProxyManager {


	private final int numberOfPorts;
	private final List<ProxyServer> proxyServers;
	private final CountDownLatch proxiesStarted;
	private final ExecutorService newFixedThreadPool;

	public ProxyManager(int numberOfPorts) {
		this.numberOfPorts = numberOfPorts;
		proxyServers = Collections
				.synchronizedList(new ArrayList<ProxyServer>());

		proxiesStarted = new CountDownLatch(numberOfPorts);
		newFixedThreadPool = Executors.newFixedThreadPool(numberOfPorts);

	}

	public void start() throws InterruptedException {

		for (int i = 0; i < numberOfPorts; i++) {

			newFixedThreadPool.execute(new Runnable() {

				@Override
				public void run() {
					try {
					    int portNumber = findAvailablePortNumber();
						ProxyServer proxyServer = new ProxyServer(portNumber);

						proxyServers.add(proxyServer);
						proxiesStarted.countDown();
						proxyServer.start();
					} catch (IOException e) {
						e.printStackTrace();
					}

				}
			});

		}

		proxiesStarted.await(10, TimeUnit.SECONDS);
	}
	
	
    // returns the first unused port number
    public static int findAvailablePortNumber() {
        int portNumber = -1;
        try {
            ServerSocket serverSocket = new ServerSocket(0);
            portNumber = serverSocket.getLocalPort();
            serverSocket.close();
        } catch (IOException ioException) {
            portNumber = -1;
        }
        return portNumber;
    }

	public void stop() {
		for (ProxyServer proxyServer : proxyServers) {

			try {
				proxyServer.stop();
			} catch (Exception e) {
			  //  TO DO
			}

		}

	}

	public List<ProxyServer> getProxyServers() {
		return Collections.unmodifiableList(proxyServers);
		
	}

}
