/*
 * Copyright 2011 by Software AG
 *
 * Uhlandstrasse 12, D-64297 Darmstadt, GERMANY
 *
 * All rights reserved
 *
 * This software is the confidential and proprietary
 * information of Software AG ('Confidential Information').
 * You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license
 * agreement you entered into with Software AG or its distributors.
 */

package com.sge.network;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class ProxyServer {

    private static final int WORKER_THREADS = 2;

    private static final String DEFAULT_REMOTE_HOST = "localhost";

    private static final int DEFAULT_REMOTE_PORT = 8092;

    private static final int DEFAULT_LISTEN_PORT = 8080;

    private final int listenPort;

    private final String remoteHost;

    private final int remotePort;

    private ExecutorService clientHandleExecutor;

    private ServerSocket serverSocket;

    private ExecutorService clientHandleExecutor2;

    public ProxyServer(int listenPort, String remoteHost, int remotePort) {
        this.listenPort = listenPort;
        this.remoteHost = remoteHost;
        this.remotePort = remotePort;

    }

    public ProxyServer(int listenPort) {
        this.listenPort = listenPort;
        this.remoteHost = DEFAULT_REMOTE_HOST;
        this.remotePort = DEFAULT_REMOTE_PORT;

    }

    public void start() throws IOException {
        clientHandleExecutor = Executors.newSingleThreadExecutor(new ThreadFactory() {

            AtomicInteger counter = new AtomicInteger(0);

            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "client handle executor thread No. " + counter.incrementAndGet());
            }
        });

        clientHandleExecutor2 = Executors.newSingleThreadExecutor();
        serverSocket = new ServerSocket(listenPort);

        System.out.println("ProxyServer listening for connections on " + listenPort);
        System.out.println("Serving " + remoteHost + ":" + remotePort);
        System.out.println("Threads in the pool " + WORKER_THREADS);

        while (true) {
            Socket clientSocket = serverSocket.accept();
            handleClient(clientSocket);

        }
    }

    private void handleClient(final Socket clientSocket) {
        System.out.println("Serving client " + clientSocket + " ...");

        clientHandleExecutor.submit(new Runnable() {

            @Override
            public void run() {
                Socket remoteHostClientSocket = null;
                try {
                    final InputStream streamFromClient = new BufferedInputStream(clientSocket.getInputStream());
                    final PrintStream streamToClient = new PrintStream(clientSocket.getOutputStream());

                    final byte[] request = new byte[1024 * 1024];
                    final byte[] reply = new byte[1024 * 1024];

                    remoteHostClientSocket = new Socket(remoteHost, remotePort);

                    // Get server streams.
                    final InputStream streamFromServer = remoteHostClientSocket.getInputStream();
                    final OutputStream streamToServer = remoteHostClientSocket.getOutputStream();

                    // a thread to read the client's requests and pass them
                    // to the server. A separate thread for asynchronous.
                    clientHandleExecutor2.submit(new Runnable() {
                        public void run() {
                            int bytesRead;
                            try {
                                while ((bytesRead = streamFromClient.read(request)) != -1) {
                                    streamToServer.write(request, 0, bytesRead);
                                    streamToServer.flush();
                                }
                            } catch (IOException e) {
                            }

                            // the client closed the connection to us, so close our
                            // connection to the server.
                            try {
                                streamToServer.close();
                            } catch (IOException e) {
                            }
                        }
                    });

                    // Read the server's responses
                    // and pass them back to the client.
                    int bytesRead;
                    try {
                        while ((bytesRead = streamFromServer.read(reply)) != -1) {
                            streamToClient.write(reply, 0, bytesRead);
                            streamToClient.flush();
                        }
                    } catch (IOException e) {
                    }

                    // The server closed its connection to us, so we close our
                    // connection to our client.
                    streamToClient.close();

                } catch (IOException e) {
                    e.printStackTrace(System.err);
                } finally {
                    try {
                        if (remoteHostClientSocket != null) remoteHostClientSocket.close();
                        if (clientSocket != null) clientSocket.close();
                    } catch (IOException e) {
                    }
                }

            }
        });

    }

    public void stop() {
        try {
            serverSocket.close();
            clientHandleExecutor.shutdown();
        } catch (Exception e) {
            e.printStackTrace(System.err);

        }

    }

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        int listenPort = DEFAULT_LISTEN_PORT;
        String remoteHost = DEFAULT_REMOTE_HOST;
        int remotePort = DEFAULT_REMOTE_PORT;
        if (args != null && args.length > 0) {
            listenPort = Integer.valueOf(args[0]);
            if (args.length > 1) {

                remoteHost = args[1];
                remotePort = Integer.valueOf(args[2]);
            }
        }

        ProxyServer proxyServer = new ProxyServer(listenPort, remoteHost, remotePort);
        proxyServer.start();

    }

}
