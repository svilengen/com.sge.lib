package com.sge.generator;

import java.util.Random;

public class RandomString {
	private static final char[] symbolsDigit = new char[10];
	private static final char[] symbolsAlpha = new char[26];

	  static {
	    for (int idx = 0; idx < 10; ++idx)
	    	symbolsDigit[idx] = (char) ('0' + idx);
	    for (int idx = 0; idx < 26; ++idx)
	    	symbolsAlpha[idx] = (char) ('a' + idx);
	  }

	  private final Random random = new Random();

	  private final char[] buf;

	  public RandomString(int length)
	  {
	    if (length < 1)
	      throw new IllegalArgumentException("length < 1: " + length);
	    buf = new char[length];
	  }

	  public String nextString()
	  {
	    for (int idx = 0; idx < buf.length; ++idx) {
	    	char c; 
	    	if (idx == 0 || idx == 1) {
	    		c = symbolsAlpha[random.nextInt(symbolsAlpha.length)];
	    		c = Character.toUpperCase(c);
	    	} else {
	    		c = symbolsDigit[random.nextInt(symbolsDigit.length)];
	    		
	    	}
	    	
			
			
			buf[idx] = c;
		}
	    return new String(buf);
	  }
}
