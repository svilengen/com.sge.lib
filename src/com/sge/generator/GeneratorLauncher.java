package com.sge.generator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

public class GeneratorLauncher {

	public static void main(String[] args) throws FileNotFoundException {
		RandomString randomString = new RandomString(8);
		
		Set<String> set = new HashSet<String>();
		Set<String> duplicates = new HashSet<String>();
		for (int i = 0; i < 1600; i++) {
			String nextString = randomString.nextString();
			while (!set.add(nextString)) {
				duplicates.add(nextString);
				nextString = randomString.nextString();
			}
			System.out.println(nextString);
		}
		
		PrintWriter printWriter = new PrintWriter(new File("d:/generated.txt"));
		
		for (String string : set) {
			printWriter.write(string);
			printWriter.write(String.format("%n"));
			
		}
		
		printWriter.close();
		
		System.out.println(set.size());
		System.out.println(duplicates.size());
		

	}

}
