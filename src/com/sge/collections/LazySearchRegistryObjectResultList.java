/*
 * Copyright 2008-2011 by Software AG
 *
 * Uhlandstrasse 12, D-64297 Darmstadt, GERMANY
 *
 * All rights reserved
 *
 * This software is the confidential and proprietary
 * information of Software AG ('Confidential Information').
 * You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license
 * agreement you entered into with Software AG or its distributors.
 */

/*++ Source Code Control System information
 * $Source: /CVSRepLG/CentraSite/src/src/resourceAccess/implementation/src/com/centrasite/resourceaccess/utilities/LazySearchRegistryObjectResultList.java,v $
 * $Revision: 1.1.1.2 $
 * $Author: bgsge $
 * $Date: 2009/06/18 12:41:29 $
 *
 * Revision history:
 * $$Log: LazySearchRegistryObjectResultList.java,v $
 * $Revision 1.1.1.2  2009/06/18 12:41:29  bgsge
 * $INM-6401 CentraSite migration fails with INOXME8501: Memory allocation failed
 * $Always close the bulk Response
 * $
 * $Revision 1.1.1.1  2009/04/24 04:00:03  odessy
 * $PS initial import
 * $
 * $Revision 1.1.2.4  2009/02/21 08:41:50  ukmtk
 * $INM2551 copyright
 * $
 * $Revision 1.1.2.3  2009/02/09 08:20:25  bgsge
 * $TRT2638 Format code using "Eclipse-SAG-Java-Diagonal" template.
 * $
 * $Revision 1.1.2.2  2009/02/04 14:35:53  bgsge
 * $TRT2638 I18N
 * $
 * $Revision 1.1.2.1  2009/02/03 10:03:29  bgsge
 * $TRT2638 Enable SSS for:
 * $UserManager.getUnregisteredUsers(String)
 * $GroupManager.getUnregisteredGroups(String)
 * $
 * $Revision 1.1.2.3  2009/01/22 12:54:01  bgsge
 * $TRT2638 Enable SSS for:
 * $UserManager.getRegisteredUsers()
 * $UserManager.getUsers(String)
 * $UserManager.getUsers(Organization)
 * $
 * $Revision 1.1.2.2  2008/11/20 14:32:59  bgmpa
 * $P311355 remove redundant calls to SIN
 * $
 * $Revision 1.1.2.1  2008/11/18 07:54:46  bgsge
 * $TRT2550 Introduce Lazy collections - not used yet.
 * $Introduce factory methods for creating resource access entities on basis principal or registry object.
 * $$
 --*/
package com.sge.collections;

import java.util.AbstractSequentialList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.xml.registry.JAXRException;
import javax.xml.registry.infomodel.RegistryObject;

import org.apache.log4j.Logger;

import com.centrasite.jaxr.CentraSiteBulkResponse;
import com.centrasite.jaxr.infomodel.Constants;
import com.centrasite.resourceaccess.managers.GroupManagerImpl;
import com.centrasite.resourceaccess.managers.PermissionManagerImpl;
import com.centrasite.resourceaccess.managers.ResourceAccessManager;
import com.centrasite.resourceaccess.managers.ResourceAccessManagerImpl;
import com.centrasite.resourceaccess.managers.RoleManagerImpl;
import com.centrasite.resourceaccess.managers.UserManagerImpl;
import com.centrasite.resourceaccess.resources.ResourceAccessType;
import com.softwareag.util.messages.ErrorMessage;

/**
 * A collection wrapping JAXR lazy response collection. On single iteration 1. registry object is loaded out of the JAXR
 * lazy response collection 2. respective resource access type instance is created, based on the registry object type
 * currently being iterated over 3. the resource access type is stored in an internal to this collection map for
 * subsequent lookup (to prevent creation of the same wrapped instance multiple times)
 * 
 * @author Svilen Genchev
 * 
 */
public class LazySearchRegistryObjectResultList<T extends ResourceAccessType> extends AbstractSequentialList<T> {
    
    private static Logger logger = Logger.getLogger(LazySearchRegistryObjectResultList.class);

    private Map<String, ResourceAccessType> roleMap_ = new HashMap<String, ResourceAccessType>();

    private List<RegistryObject> lazyRegistryObjectsCollection_;

    private final ResourceAccessManager ram_;

    private CentraSiteBulkResponse bulkResponse_;

    public LazySearchRegistryObjectResultList(CentraSiteBulkResponse bulkResponse,
        ResourceAccessManager ram) throws JAXRException {
        this((Collection<RegistryObject>) bulkResponse.getCollection(), ram);                
        
        GrupUtilities.validateBulkResponse(bulkResponse);
        
        bulkResponse_ = bulkResponse;
    }
    
    public LazySearchRegistryObjectResultList(Collection<RegistryObject> lazyRegistryObjectsCollection,
        ResourceAccessManager ram) {
        if (lazyRegistryObjectsCollection instanceof List) {
            lazyRegistryObjectsCollection_ = (List<RegistryObject>) lazyRegistryObjectsCollection;
        }
        else {

            String errMsg = new ErrorMessage("INMRAE0014").getMessage(((ResourceAccessManagerImpl) ram).getLocale());
            throw new IllegalArgumentException(errMsg);
        }
        ram_ = ram;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return new LazyRegistryObjectListIterator<T>(index);
    }

    @Override
    public int size() {
        return lazyRegistryObjectsCollection_.size();
    }
    
    

    @Override
    protected void finalize() throws Throwable {
        JaxrUtilities.closeBulkResponse(bulkResponse_);
    }



    protected class LazyRegistryObjectListIterator<P extends ResourceAccessType> implements ListIterator<P> {
        private int currentIndex_;

        private ListIterator<RegistryObject> roLazyListIterator_;

        public LazyRegistryObjectListIterator(int index) {
            if ((index != 0) && ((index < 0) || (index > size()))) {
                throw new IndexOutOfBoundsException();
            }
            currentIndex_ = index - 1;
            roLazyListIterator_ = lazyRegistryObjectsCollection_.listIterator(index);

        }

        public P next() {
            int index = nextIndex();
            if (index < size()) {
                currentIndex_ = index;

                RegistryObject ro = (RegistryObject) roLazyListIterator_.next();
                ResourceAccessType resourceAccessEntity = null;
                try {

                    resourceAccessEntity = roleMap_.get(ro.getKey().getId());
                    if (resourceAccessEntity == null) {

                        String roTypeId = ro.getObjectType().getKey().getId();

                        if (Constants.OBJECT_TYPE_KEY_User.equals(roTypeId)) {
                            resourceAccessEntity = ((UserManagerImpl) ram_.getUserManager()).createUser(ro);
                        }
                        else if (Constants.OBJECT_TYPE_KEY_Role.equals(roTypeId)) {
                            resourceAccessEntity = ((RoleManagerImpl) ram_.getRoleManager()).createRole(ro);
                        }
                        else if (Constants.OBJECT_TYPE_KEY_Permission.equals(roTypeId)) {
                            resourceAccessEntity = ((PermissionManagerImpl) ram_.getPermissionManager())
                                .createPermission(ro);
                        }
                        else if (Constants.OBJECT_TYPE_KEY_Group.equals(roTypeId)) {
                            resourceAccessEntity = ((GroupManagerImpl) ram_.getGroupManager()).createGroup(ro);
                        }

                        roleMap_.put(ro.getKey().getId(), resourceAccessEntity);
                    }
                }
                catch (Exception e) {
                    throw new RuntimeException(e);
                }

                return (P) resourceAccessEntity;
            }
            else {
                throw new NoSuchElementException();
            }
        }

        public boolean hasNext() {
            boolean hasNext = roLazyListIterator_.hasNext();
            if (!hasNext) {
                try {
                    JaxrUtilities.closeBulkResponse(bulkResponse_);
                }
                catch (JAXRException e) {
                    logger.error("Error while closing the Bulk Response", e);
                }
            }
            return hasNext;
        }

        public int nextIndex() {
            int index = currentIndex_ + 1;
            return Math.min(index, size());
        }

        public int previousIndex() {
            throw new UnsupportedOperationException();
        }

        public boolean hasPrevious() {
            throw new UnsupportedOperationException();
        }

        public P previous() {
            throw new UnsupportedOperationException();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public void add(P o) {
            throw new UnsupportedOperationException();
        }

        public void set(P o) {
            throw new UnsupportedOperationException();
        }
    }

}
