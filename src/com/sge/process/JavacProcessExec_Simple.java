/*
 * Copyright 2008-2009 by Software AG
 *
 * Uhlandstrasse 12, D-64297 Darmstadt, GERMANY
 *
 * All rights reserved
 *
 * This software is the confidential and proprietary
 * information of Software AG ('Confidential Information').
 * You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license
 * agreement you entered into with Software AG or its distributors.
 */
package com.sge.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author Svilen Genchev
 *
 */
public class JavacProcessExec_Simple {
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    private static boolean isWindows() {
        return File.pathSeparatorChar == ';';
    }

    /**
     * @param args
     * @throws IOException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        // construct javac execution string

        

        if (args.length == 0) {
            throw new IllegalArgumentException("please provide root directory.");
        }

        File root = new File(args[0]);
        File srcDir = new File(root, "src");
        File classesDir = new File(root, "classes");
        File comPackage = new File(srcDir, "com");
        File implFile = new File(comPackage, "ToBeCompiled.java");
        

        StringBuilder javacExecutionString = new StringBuilder();

        String pathSeparator = File.pathSeparator;

        String classPathString = System.getProperty("java.class.path");
        StringBuilder classPath = new StringBuilder();
        
        System.out.println("java.class.path value");
        System.out.println(classPathString);

        System.out.println("isWindows = " + isWindows());

        if (isWindows()) {
            javacExecutionString.append("javac ");
            javacExecutionString.append("-sourcepath ");
            javacExecutionString.append("\"" + srcDir.toString() + "\" ");
            javacExecutionString.append("-classpath ");
            javacExecutionString.append("\"");
            javacExecutionString.append(classesDir.toString());
            javacExecutionString.append("\"");
            javacExecutionString.append(pathSeparator);
            javacExecutionString.append("\"");
            javacExecutionString.append(classPathString.replaceAll(pathSeparator, "\"" + pathSeparator + "\""));
            javacExecutionString.append("\"");
            javacExecutionString.append(" ");
            javacExecutionString.append("\"" + implFile.toString() + "\" ");
            javacExecutionString.append("-d ");
            javacExecutionString.append("\"" + classesDir + "\"");
            
            
        }
        else {
            javacExecutionString.append("javac ");
            javacExecutionString.append("-sourcepath ");
            javacExecutionString.append(srcDir.toString() + " ");
            javacExecutionString.append("-classpath ");
            javacExecutionString.append(classesDir.toString());            
            javacExecutionString.append(pathSeparator);            
            javacExecutionString.append(classPathString);
            javacExecutionString.append(" ");
            javacExecutionString.append(implFile.toString() + " ");
            javacExecutionString.append("-d ");
            javacExecutionString.append(classesDir);
            
        }

        System.out.println("<JAVAC_EXECUTION_STRING>");
        System.out.println(javacExecutionString.toString());
        System.out.println("</JAVAC_EXECUTION_STRING>");

        // execute
        Process javacProcess = Runtime.getRuntime().exec(javacExecutionString.toString());

        // read output
        readProcessOutput(javacProcess);

    }

    private static int readProcessOutput(Process javacProcess) throws IOException, InterruptedException {
        InputStream stderr = javacProcess.getErrorStream();
        InputStreamReader isr = new InputStreamReader(stderr);
        BufferedReader br = new BufferedReader(isr);
        String line = null;

        StringBuilder sb = new StringBuilder();
        sb.append("<COMPILER-ERROR>");
        sb.append(LINE_SEPARATOR);
        while ((line = br.readLine()) != null) {
            sb.append(line);
            sb.append(LINE_SEPARATOR);
        }
        sb.append("</COMPILER-ERROR>");
        sb.append(LINE_SEPARATOR);

        int exitVal = javacProcess.waitFor();
        if (exitVal != 0) {
            System.out.println(sb.toString());
        }

        return exitVal;
    }

}
