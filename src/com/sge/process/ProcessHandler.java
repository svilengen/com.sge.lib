package com.sge.process;

import java.io.InputStream;


public class ProcessHandler
{
    /**
     * Method to perform a "wait" for a process and return its exit value. This is a workaround for <CODE>process.waitFor()</CODE>
     * never returning.
     */
    public static int doWaitFor(Process p)
    {
        int exitValue = -1; // returned to caller when p is finished
        try
        {
            InputStream in = p.getInputStream();
            InputStream err = p.getErrorStream();
            boolean finished = false; // Set to true when p is finished
            while (!finished)
            {
                try
                {
                    while (in.available() > 0)
                    {
                        // Print the output of our system call
                        Character c = new Character((char)in.read());
                        System.out.print(c);
                    }
                    while (err.available() > 0)
                    {
                        // Print the output of our system call
                        Character c = new Character((char)err.read());
                        System.out.print(c);
                    }
                    // Ask the process for its exitValue. If the process
                    // is not finished, an IllegalThreadStateException
                    // is thrown. If it is finished, we fall through and
                    // the variable finished is set to true.
                    exitValue = p.exitValue();
                    finished = true;
                }
                catch (IllegalThreadStateException e)
                {
                    // Process is not finished yet;
                    // Sleep a little to save on CPU cycles
                    Thread.currentThread().sleep(500);
                }
            }
        }
        catch (Exception e)
        {
            // unexpected exception! print it out for debugging...
            System.err.println("doWaitFor(): unexpected exception - " + e.getMessage());
        }
        // return completion status to caller
        return exitValue;
    }
}
