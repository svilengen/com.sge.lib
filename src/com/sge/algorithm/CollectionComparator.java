/*
 * Copyright 2011 by Software AG
 *
 * Uhlandstrasse 12, D-64297 Darmstadt, GERMANY
 *
 * All rights reserved
 *
 * This software is the confidential and proprietary
 * information of Software AG ('Confidential Information').
 * You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license
 * agreement you entered into with Software AG or its distributors.
 */

package com.sge.algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class CollectionComparator {
    
    /**
     * Compares two collections by invoking {@link ChangeHandler} callback methods.
     * @param coll1 The collection against which comparison will be made.
     * @param coll2 The collection which is going to be compared against coll1.
     * @param changeHandler The callback interface invoked when difference between the collections is found.
     * @param updateComparator Used to compare two elements and decide whether the latter element is a updated version
     * of the former.
     */
    public <T> void compareLists(Collection<T> coll1, Collection<T> coll2, ChangeHandler<T> changeHandler,
        UpdateComparator<T> updateComparator) {
        final List<T> existingObjects = new ArrayList<T>();
        existingObjects.addAll(coll1);

        // invoke the ChangeHandler
        for (T element : coll2) {
            int indexOf = existingObjects.indexOf(element);
            if (indexOf == -1) {
                changeHandler.created(element);
            } else {
                if (updateComparator != null && updateComparator.isUpdated(existingObjects.get(indexOf), element)) {
                    changeHandler.updated(element);
                }
            }
            existingObjects.remove(element);

        }

        // if some objects are left unchecked then they are deleted
        for (T element : existingObjects) {
            changeHandler.deleted(element);
        }

    }

    /**
     * Provides callback mechanism to
     * {@link InventoryChangedEventHandler#compareLists(Collection, Collection, ChangeHandler)} . Currently supports
     * "created", "deleted" and "updated" callbacks.
     * 
     * @param <T> Generic type argument.
     */
    public static class ChangeHandler<T> {

        private Set<T> created = new LinkedHashSet<T>();

        private Set<T> deleted = new LinkedHashSet<T>();

        private Set<T> updated = new LinkedHashSet<T>();

        public Collection<T> getCreated() {
            return Collections.unmodifiableCollection(created);
        }

        public Collection<T> getDeleted() {
            return Collections.unmodifiableCollection(deleted);
        }

        public Collection<T> getUpdated() {
            return Collections.unmodifiableCollection(updated);
        }

        void created(T element) {
            created.add(element);
        }

        void deleted(T element) {
            deleted.add(element);
        }

        void updated(T element) {
            updated.add(element);
        }

    }

    /**
     * Used to compare two elements and decide whether the latter element is a updated version of the former.
     * 
     * @param <T> Generic type argument.
     */
    public static interface UpdateComparator<T> {

        /**
         * Compare two elements and decide whether the latter element is a updated version of the former
         * @param obj1
         * @param obj2
         * @return True of obj2 is updated version of obj1.
         */
        boolean isUpdated(T obj1, T obj2);

    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }
    
    
    @Test
    public void testStringListCompareCreated() {
        List<String> list1 = Arrays.asList(new String[] { "e1", "e2", "e3" });
        List<String> list2 = Arrays.asList(new String[] { "e1", "e2", "e3", "e4", "e5" });
        ChangeHandler<String> changeHandler = new ChangeHandler<String>();
        compareLists(list1, list2, changeHandler, null);

        Assert.assertArrayEquals(new String[] { "e4", "e5" }, changeHandler.getCreated().toArray());
        Assert.assertArrayEquals(new String[] {}, changeHandler.getDeleted().toArray());
    }

    
    @Test
    public void testStringListCompareDeletedCreated() {
        List<String> list1 = Arrays.asList(new String[] { "e1", "e2", "e3" });
        List<String> list2 = Arrays.asList(new String[] { "e1", "e4", "e5" });
        ChangeHandler<String> changeHandler = new ChangeHandler<String>();
        compareLists(list1, list2, changeHandler, null);

        Assert.assertArrayEquals(new String[] { "e4", "e5" }, changeHandler.getCreated().toArray());
        Assert.assertArrayEquals(new String[] { "e2", "e3" }, changeHandler.getDeleted().toArray());
    }

}
