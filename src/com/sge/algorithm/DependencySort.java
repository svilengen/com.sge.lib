/*+++*******************************************************************
 * SC-SYSTEM DESCRIPTION (RCS)
 *
 *  $Source: /CVSInoRep/CentraSite/src/src/Control/PluggableUI/src/com/softwareag/cis/plugin/registry/Attic/DependencySort.java,v $
 *  $Revision: 1.1.2.7 $
 *  $Author: rmu $
 *  $Date: 2009/01/28 09:47:04 $
 ***********************************************************************/

package com.sge.algorithm;


import java.io.Serializable;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.softwareag.cis.plugin.registry.PlugIn;


public class DependencySort
{
  private static Logger logger = Logger.getLogger(DependencySort.class);

  private List<PlugIn> plugIns = null;
  private Stack<String> sortedQueue = null;
  private Map<String, Integer> inDegrees = null;


  public DependencySort(List<PlugIn> plugIns)
  {
    this.plugIns = plugIns;
    sortedQueue = new Stack<String>();
    inDegrees = new HashMap<String, Integer>(getPlugIns().size());
    // initialize sort algorithm
    for (PlugIn plugIn : plugIns)
    {
      initInDegree(plugIn);
    }
  }


  private List<PlugIn> getPlugIns()
  {
    return plugIns;
  }


  public Stack<String> getSortedIDs()
  {
    return sortedQueue;
  }


  /**
   * Sort plug-ins on basis dependency Reverse Topological Sort (Acyclic Directed Graph) algorithm
   */
  public void sort()
  {
    // initial sources (nodes/plug-ins which are not required by any nodes/plug-ins)
    List<String> sources = getSources();

    while (!(sources.isEmpty()))
    {
      // just pick one of the source nodes; a lot of permutations possible
      String sourceID = (String)sources.remove(sources.size() - 1);
      // add it to a stack (LIFO)
      sortedQueue.add(sourceID);
      // remove the processed source from the in-degree table
      inDegrees.remove(sourceID);
      // get the source node by ID
      PlugIn plugin = getPlugIn(sourceID);
      // decrease the in-degree of the required plug-ins
      for (String requiredPluginID : plugin.getRequiredPluginIDs())
      {
        int inDegree = inDegrees.get(requiredPluginID).intValue();
        inDegree--;
        if (inDegree == 0)
        {
          sources.add(requiredPluginID);
        }
        inDegrees.put(requiredPluginID, Integer.valueOf(inDegree));
      }
    }

    // if unprocessed nodes (plug-ins) remain then a cyclic dependency is present
    if (!inDegrees.isEmpty())
    {
      throw new CyclicDependencyException();
    }

    // assign dependency order to each plug-in
    Stack<String> sortedPlugInIDs = getSortedIDs();
    int order = 0;
    while (!sortedPlugInIDs.empty())
    {
      String plugInID = sortedPlugInIDs.pop();
      PlugIn plugIn = getPlugIn(plugInID);
      plugIn.setPlugInDependencyOrder(order);
      logger.info("Plug-in: " + plugInID + " with dependency order: " + order);
      order++;
    }
  }


  private PlugIn getPlugIn(String sourceID)
  {
    for (PlugIn plugIn : plugIns)
    {
      if (plugIn.getPlugInId().equalsIgnoreCase(sourceID))
      {
        return plugIn;
      }
    }
    return null;
  }


  /**
   * Traverse the in-degree table searching for plug-ins with in-degree of 0
   * @return list of sources (nodes with in-degree of 0)
   */
  private List<String> getSources()
  {
    List<String> sources = new LinkedList<String>();
    Set<Entry<String, Integer>>  entrySet = inDegrees.entrySet();
    for (Iterator<Entry<String, Integer>> iterator = entrySet.iterator(); iterator.hasNext();)
    {
      Entry<String, Integer> entry = iterator.next();
      // source node should have in-degree of 0
      if (entry.getValue().intValue() == 0)
      {
        sources.add(entry.getKey());
      }
    }
    return sources;
  }


  /**
   * Calculate the in-degree (number of times being required) of a plug-in
   * @return number of times being required
   */
  private int initInDegree(PlugIn plugIn)
  {
    int inDeg = 0;
    for (PlugIn requiredBy : plugIns)
    {
      if (requiredBy.getRequiredPluginIDs().contains(plugIn.getPlugInId()))
      {
        inDeg++;
      }
    }
    inDegrees.put(plugIn.getPlugInId(), Integer.valueOf(inDeg));
    return inDeg;
  }


  public static class CyclicDependencyException extends RuntimeException
  {
    private static final long serialVersionUID = -2869595619876993887L;

    public CyclicDependencyException(String message)
    {
      super(message);
    }

    public CyclicDependencyException()
    {
      super("Cyclic dependency detected!");
    }
  }


  public static class PlugInDependencyComparator implements Comparator<PlugIn>, Serializable
  {
    private static final long serialVersionUID = 1L;

    public int compare(PlugIn p1, PlugIn p2)
    {
      int plugIn1Order = p1.getPlugInDependencyOrder();
      int plugIn2Order = p2.getPlugInDependencyOrder();
      if (plugIn1Order == plugIn2Order)
      {
        if (logger.isDebugEnabled())
          logger.debug("ExtensionComparator.compare() plugIn1 = " + p1 + ", plugIn2 = " + p2 + " : return -1");
        return -1;
      }
      else
      {
        if (plugIn1Order < plugIn2Order)
        {
          if (logger.isDebugEnabled())
            logger.debug("ExtensionComparator.compare() plugIn1 = " + p1 + ", plugIn2 = " + p2 + " : return -1");
          return -1;
        }
        else
        {
          if (logger.isDebugEnabled())
            logger.debug("ExtensionComparator.compare() plugIn1 = " + p1 + ", plugIn2 = " + p2 + " : return 1");
          return 1;
        }
      }
    }
  }

}
