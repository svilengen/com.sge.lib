package com.sge.security;

import java.security.BasicPermission;
import java.security.Permission;
import java.security.PermissionCollection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

/**
 * @see Permission
 * @author bgsge
 * @deprecated Use {@link CentraSitePermission} instead. NOTE: Still used internally for TRT2492. Do not remove it yet.
 * 
 */
public class BasePermission extends BasicPermission {

    private static final String NAME_MANAGE_ALL_ASSETS = "PermissionManageAllAssets";

    private static final String NAME_CREATE_ALL_ASSETS = "PermissionCreateAllAssets";

    private static final String NAME_VIEW_ALL_ASSETS = "PermissionViewAllAssets";

    private static final String NAME_MANAGE_ALL_ORGANIZATIONS = "PermissionManageAllOrganizations";

    private static final String NAME_MANAGE_ALL_SUB_ORGANIZATIONS = "PermissionManageAllSubOrganizations";

    private static final String NAME_MANAGE_ALL_USERS = "PermissionManageAllUsers";

    private static final String NAME_MANAGE_ALL_GROUPS = "PermissionManageAllGroups";

    private static final String NAME_MANAGE_ROLES = "PermissionManageRoles";

    private static final String NAME_MANAGE_SYSTEM_WIDE_ROLES = "PermissionManageSystemWideRoles";

    private static final String NAME_MANAGE_ALL_SUPPORTING_DOCUMENTS = "PermissionManageAllSupportingDocuments";

    private static final String NAME_MANAGE_POLICIES = "PermissionManagePolicies";

    private static final String NAME_MANAGE_SYSTEM_WIDE_POLICIES = "PermissionManageSystemWidePolicies";

    private static final String NAME_MANAGE_ALL_POLICY_ACTIONS = "PermissionManageAllPolicyActions";

    private static final String NAME_MANAGE_LIFECYCLE_MODELS = "PermissionManageLifecycleModels";

    private static final String NAME_MANAGE_SYSTEM_WIDE_LIFECYCLE_MODELS = "PermissionManageSystemWideLifecycleModels";

    private static final String NAME_MANAGE_ALL_REPORTS = "PermissionManageAllReports";

    private static final String NAME_MANAGE_ALL_TAXONOMY = "PermissionManageAllTaxonomies";

    private static final String NAME_MANAGE_ALL_TARGET_TYPE = "PermissionManageAllTargetTypes";

    private static final String NAME_MANAGE_ALL_TARGETS = "PermissionManageAllTargets";

    private static final String NAME_MANAGE_ALL_CONSUMER_APPLICATIONS = "PermissionManageAllConsumerApplications";

    private static final String NAME_VIEW_APPROVAL_HISTORY = "PermissionViewApprovalHistory";

    private static final String NAME_MANAGE_REGISTRY_SETTINGS = "PermissionManageRegistrySettings";

    private static final String NAME_MANAGE_UDDI_SUBSCRIPTIONS = "PermissionManageUDDISubscriptions";

    private static final String NAME_VIEW_SYSTEM_AUDIT_LOG = "PermissionViewSystemAuditLog";

    private static final String NAME_VIEW_POLICY_LOG = "PermissionViewPolicyLog";

    private static final String NAME_MANAGE_ASSET_TYPES = "PermissionManageAssetTypes";

    private static final String NAME_REGISTER_AS_CONSUMER = "PermissionRegisterAsConsumer";

    private static final String NAME_USE_THE_HOME_UI = "PermissionUseHomeUI";

    private static final String NAME_USE_THE_BROWSE_AND_SEARCH_UI = "PermissionUseBrowseAndSearchUI";

    private static final String NAME_USE_THE_OPERATIONS_UI = "PermissionUseOperationsUI";

    private static final String NAME_USE_THE_POLICY_UI = "PermissionUsePolicyUI";

    private static final String NAME_USE_THE_LCM_UI = "PermissionUseLCMUI";

    private static final String NAME_USE_ADMINISTRATION_UI = "PermissionUseAdministrationUI";

    private static final String NAME_USE_REPORTS_UI = "PermissionUseReportsUI";

    private static final String NAME_USE_SMH_UI = "PermissionUseSystemManagementUI";

    // Type-level Permissions
    public static final BasePermission MANAGE_ALL_ASSETS = new BasePermission(NAME_MANAGE_ALL_ASSETS);

    public static final BasePermission CREATE_ALL_ASSETS = new BasePermission(NAME_CREATE_ALL_ASSETS);

    public static final BasePermission VIEW_ALL_ASSETS = new BasePermission(NAME_VIEW_ALL_ASSETS);

    public static final BasePermission MANAGE_ALL_ORGANIZATIONS = new BasePermission(NAME_MANAGE_ALL_ORGANIZATIONS);

    public static final BasePermission MANAGE_ALL_SUB_ORGANIZATIONS = new BasePermission(
        NAME_MANAGE_ALL_SUB_ORGANIZATIONS);

    public static final BasePermission MANAGE_ALL_USERS = new BasePermission(NAME_MANAGE_ALL_USERS);

    public static final BasePermission MANAGE_ALL_GROUPS = new BasePermission(NAME_MANAGE_ALL_GROUPS);

    public static final BasePermission MANAGE_ROLES = new BasePermission(NAME_MANAGE_ROLES);

    public static final BasePermission MANAGE_SYSTEM_WIDE_ROLES = new BasePermission(NAME_MANAGE_SYSTEM_WIDE_ROLES);

    public static final BasePermission MANAGE_ALL_SUPPORTING_DOCUMENTS = new BasePermission(
        NAME_MANAGE_ALL_SUPPORTING_DOCUMENTS);

    public static final BasePermission MANAGE_POLICIES = new BasePermission(NAME_MANAGE_POLICIES);

    public static final BasePermission MANAGE_SYSTEM_WIDE_POLICIES = new BasePermission(
        NAME_MANAGE_SYSTEM_WIDE_POLICIES);

    public static final BasePermission MANAGE_ALL_POLICY_ACTIONS = new BasePermission(NAME_MANAGE_ALL_POLICY_ACTIONS);

    public static final BasePermission MANAGE_LIFECYCLE_MODELS = new BasePermission(NAME_MANAGE_LIFECYCLE_MODELS);

    public static final BasePermission MANAGE_SYSTEM_WIDE_LIFECYCLE_MODELS = new BasePermission(
        NAME_MANAGE_SYSTEM_WIDE_LIFECYCLE_MODELS);

    public static final BasePermission MANAGE_ALL_REPORTS = new BasePermission(NAME_MANAGE_ALL_REPORTS);

    public static final BasePermission MANAGE_ALL_TAXONOMY = new BasePermission(NAME_MANAGE_ALL_TAXONOMY);

    public static final BasePermission MANAGE_ALL_TARGET_TYPE = new BasePermission(NAME_MANAGE_ALL_TARGET_TYPE);

    public static final BasePermission MANAGE_ALL_TARGETS = new BasePermission(NAME_MANAGE_ALL_TARGETS);

    public static final BasePermission MANAGE_ALL_CONSUMER_APPLICATIONS = new BasePermission(
        NAME_MANAGE_ALL_CONSUMER_APPLICATIONS);

    // System-level permissions
    public static final BasePermission VIEW_APPROVAL_HISTORY = new BasePermission(NAME_VIEW_APPROVAL_HISTORY);

    public static final BasePermission MANAGE_REGISTRY_SETTINGS = new BasePermission(NAME_MANAGE_REGISTRY_SETTINGS);

    public static final BasePermission MANAGE_UDDI_SUBSCRIPTIONS = new BasePermission(NAME_MANAGE_UDDI_SUBSCRIPTIONS);

    public static final BasePermission VIEW_SYSTEM_AUDIT_LOG = new BasePermission(NAME_VIEW_SYSTEM_AUDIT_LOG);

    public static final BasePermission VIEW_POLICY_LOG = new BasePermission(NAME_VIEW_POLICY_LOG);

    public static final BasePermission MANAGE_ASSET_TYPES = new BasePermission(NAME_MANAGE_ASSET_TYPES);

    public static final BasePermission REGISTER_AS_CONSUMER = new BasePermission(NAME_REGISTER_AS_CONSUMER);

    public static final BasePermission USE_THE_HOME_UI = new BasePermission(NAME_USE_THE_HOME_UI);

    public static final BasePermission USE_THE_BROWSE_AND_SEARCH_UI = new BasePermission(
        NAME_USE_THE_BROWSE_AND_SEARCH_UI);

    public static final BasePermission USE_THE_OPERATIONS_UI = new BasePermission(NAME_USE_THE_OPERATIONS_UI);

    public static final BasePermission USE_THE_POLICY_UI = new BasePermission(NAME_USE_THE_POLICY_UI);

    public static final BasePermission USE_THE_LCM_UI = new BasePermission(NAME_USE_THE_LCM_UI);

    public static final BasePermission USE_ADMINISTRATION_UI = new BasePermission(NAME_USE_ADMINISTRATION_UI);

    public static final BasePermission USE_REPORTS_UI = new BasePermission(NAME_USE_REPORTS_UI);

    public static final BasePermission USE_SMH_UI = new BasePermission(NAME_USE_SMH_UI);

    public static final String PREFIX_PERMISSION = "Permission";

    /**
     * View action.
     */
    private final static int VIEW = 0x1;

    /**
     * Execute action.
     */
    private final static int EXECUTE = 0x2;

    /**
     * All actions.
     */
    // private final static int MANAGE = VIEW|EXECUTE;

    /**
     * No actions.
     */
    private final static int NONE = 0x0;

    protected int mask;

    public BasePermission(String name) {
        super(name);
        // TODO Auto-generated constructor stub
    }

    public BasePermission(String name, String actions) {
        super(name);
        parse(actions);
    }

    @Override
    public boolean implies(java.security.Permission p) {
        if (!(p instanceof BasePermission)) return false;

        BasePermission that = (BasePermission) p;

        // we get the effective mask. i.e., the "and" of this and that.
        // They must be equal to that.mask for implies to return true.
        // Check for wildcard pattern name match.

        return ((this.mask & that.mask) == that.mask) && super.implies(that);
    }

    private void parse(String action) {
        StringTokenizer st = new StringTokenizer(action, ",\t ");
        mask = NONE;
        while (st.hasMoreTokens()) {
            String tok = st.nextToken();
            if (tok.equals("view")) {
                mask |= VIEW;
            }
            else if (tok.equals("execute")) {
                mask |= EXECUTE;
            }
            else {
                throw new IllegalArgumentException("Unknown action " + tok);
            }
        }
    }

    public String getActions() {
        if (mask == 0) {
            return "";
        }
        else if (mask == VIEW) {
            return "view";
        }
        else if (mask == EXECUTE) {
            return "execute";
        }
        else if (mask == (VIEW | EXECUTE)) {
            return "view, execute";
        }
        else {
            throw new IllegalArgumentException("Unknown mask");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof BasePermission)) return false;

        BasePermission p = (BasePermission) o;
        return ((p.getName().equals(getName())) && (p.mask == mask));
    }

    @Override
    public int hashCode() {
        return getName().hashCode() ^ mask;
    }

    @Override
    public PermissionCollection newPermissionCollection() {
        return new BasePermissionCollection();
    }

    public static class BasePermissionCollection extends PermissionCollection {

        private List<BasePermission> permissions;

        public BasePermissionCollection() {
            permissions = new ArrayList<BasePermission>();
        }

        @Override
        public void add(java.security.Permission permission) {
            if (!(permission instanceof BasePermission)) throw new IllegalArgumentException("Wrong permission type");
            BasePermission xyz = (BasePermission) permission;

            String name = xyz.getName();

            for (BasePermission p : permissions) {

                if (p.getName().equals(name)) {
                    xyz = merge(xyz, p);
                }
            }

            permissions.add(xyz);
        }

        @Override
        public Enumeration elements() {
            return Collections.enumeration(permissions);
        }

        @Override
        public boolean implies(java.security.Permission permission) {
            if (!(permission instanceof BasePermission)) return false;
            BasePermission xyz = (BasePermission) permission;

            boolean implies = false;

            Iterator iter = permissions.iterator();

            while (!implies && iter.hasNext()) {
                BasePermission inList = (BasePermission) iter.next();
                implies = inList.implies(xyz);
            }

            return implies;
        }

        private BasePermission merge(BasePermission a, BasePermission b) {
            String aAction = a.getActions();
            if (aAction.equals("")) return b;
            String bAction = b.getActions();
            if (bAction.equals("")) return a;
            return new BasePermission(a.getName(), aAction + "," + bAction);
        }

    }

}
