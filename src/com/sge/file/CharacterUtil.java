/*
 * Copyright 2012 by Software AG
 *
 * Uhlandstrasse 12, D-64297 Darmstadt, GERMANY
 *
 * All rights reserved
 *
 * This software is the confidential and proprietary
 * information of Software AG ('Confidential Information').
 * You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license
 * agreement you entered into with Software AG or its distributors.
 */

package com.sge.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class CharacterUtil {

    public static void convert(File fileSource, String sourceEncoding, File fileTarget, String targetEncoding)
        throws FileNotFoundException, UnsupportedEncodingException, IOException {
        FileInputStream fis = new FileInputStream(fileSource);
        FileOutputStream fileOutputStream = new FileOutputStream(fileTarget);

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis, sourceEncoding));

        try {
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                byte[] bytes = line.getBytes(targetEncoding);
                fileOutputStream.write(bytes);

            }
        } finally {
            fis.close();
            bufferedReader.close();
            fileOutputStream.close();

        }

    }
    
    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException, IOException {
        File source = new File("D:/DevEnv/spm/trunk/modules/configuration/src/test/resources/log_config.xml");
        File target = new File("d:/log_config_UTF_16BE.xml");
       
        convert(source, "UTF-8", target, "UTF-16BE");
       
        
    }

}
