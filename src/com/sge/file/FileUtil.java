/*
 * Copyright 2008-2009 by Software AG
 *
 * Uhlandstrasse 12, D-64297 Darmstadt, GERMANY
 *
 * All rights reserved
 *
 * This software is the confidential and proprietary
 * information of Software AG ('Confidential Information').
 * You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license
 * agreement you entered into with Software AG or its distributors.
 */
package com.sge.file;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.apache.log4j.Logger;



/**
 * @author Svilen Genchev
 * 
 */
public class FileUtil {

    private static Logger logger = Logger.getLogger(FileUtil.class);

    /**
     * Delete given file and (if directory) all directories and files therein recursively
     * @param fileOrDir file or directory to be deleted
     * @param failedToDelete holds all files which were not deleted
     * @throws IOException
     */
    public static void deleteRecursive(File fileOrDir, List<File> failedToDelete) throws IOException {
        if (logger.isDebugEnabled())
            logger.debug("FileUtil.deleteRecursive() fileOrDir=" + fileOrDir + ", failedToDelete=" + failedToDelete);

        if (fileOrDir.isDirectory()) {
            File[] files = fileOrDir.listFiles();
            for (int i = 0; i < files.length; i++) {
                deleteRecursive(files[i], failedToDelete);
            }
        }

        boolean result = fileOrDir.delete();
        if (!result) {
            logger.warn("Failed to delete " + fileOrDir + ".");
            if (failedToDelete != null) {
                failedToDelete.add(fileOrDir);
            }
        }
        else {
            if (logger.isInfoEnabled()) logger.info("Successfully deleted " + fileOrDir);
        }
    }

    /**
     * Synchronized stream copy The method do not allow other threads to read from the input or write to the output
     * while copying is taking place
     * @param in InputStream
     * @param out OutputStream
     * @throws IOException
     */
    public static void streamCopy(InputStream in, OutputStream out) throws IOException {
        // do not allow other threads to read from the input
        // or write to the output while copying is taking place
        if (logger.isDebugEnabled()) logger.debug("InstallPlugInCommand.streamCopy() in, out");
        synchronized (in) {
            synchronized (out) {
                byte[] buffer = new byte[256];
                while (true) {
                    int bytesRead = in.read(buffer);
                    if (bytesRead == -1) break;
                    out.write(buffer, 0, bytesRead);
                }
            }
        }
    }

}
