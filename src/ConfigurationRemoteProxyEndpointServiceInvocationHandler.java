/*
 * Copyright 2011 by Software AG
 *
 * Uhlandstrasse 12, D-64297 Darmstadt, GERMANY
 *
 * All rights reserved
 *
 * This software is the confidential and proprietary
 * information of Software AG ('Confidential Information').
 * You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license
 * agreement you entered into with Software AG or its distributors.
 */



import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import sun.misc.*;

import org.eclipse.ecf.core.ContainerFactory;
import org.eclipse.ecf.core.IContainer;
import org.eclipse.ecf.core.security.ConnectContextFactory;
import org.eclipse.ecf.core.util.ECFException;
import org.eclipse.ecf.remoteservice.IRemoteService;
import org.eclipse.ecf.remoteservice.IRemoteServiceRegistration;
import org.eclipse.ecf.remoteservice.client.IRemoteCallParameter;
import org.eclipse.ecf.remoteservice.client.IRemoteCallable;
import org.eclipse.ecf.remoteservice.client.IRemoteResponseDeserializer;
import org.eclipse.ecf.remoteservice.client.IRemoteServiceClientContainerAdapter;
import org.eclipse.ecf.remoteservice.client.RemoteCallParameterFactory;
import org.eclipse.ecf.remoteservice.rest.IRestCall;
import org.eclipse.ecf.remoteservice.rest.RestCallFactory;
import org.eclipse.ecf.remoteservice.rest.RestCallableFactory;
import org.eclipse.ecf.remoteservice.rest.client.HttpGetRequestType;
import org.eclipse.ecf.remoteservice.rest.client.HttpPostRequestType;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

import com.softwareag.platform.management.client.remote.proxy.BaseRemoteResponseDeserializer;
import com.softwareag.platform.management.client.remote.proxy.ExceptionHandler;
import com.softwareag.platform.management.client.remote.proxy.RemoteServiceProxy;
import com.softwareag.platform.management.client.remote.proxy.Request;
import com.softwareag.platform.management.client.remote.proxy.UriContainer;
import com.softwareag.platform.management.client.remote.proxy.configuration.ConfigurationTypeDeserializer;
import com.softwareag.platform.management.client.remote.proxy.configuration.ConfiugrationInstanceDeserializer;
import com.softwareag.platform.management.common.component.ServiceComponent;
import com.softwareag.platform.management.common.configuration.ConfigurationSerializer;
import com.softwareag.platform.management.common.configuration.ConfigurationService;
import com.softwareag.platform.management.common.configuration.ConfigurationStatus;
import com.softwareag.platform.management.common.configuration.ConfigurationType;
import com.softwareag.platform.management.common.exceptions.PlatformManagerException;
import com.softwareag.platform.management.util.ServiceComponentsUtil;
import com.softwareag.platform.management.util.StreamUtil;

public class ConfigurationRemoteProxyEndpointServiceInvocationHandler implements InvocationHandler {

    public static final int INPUT_STREAM_REQUEST_ENTITY = 0;

    public static final int STRING_REQUEST_ENTITY = 1;

    public static final int FILE_REQUEST_ENTITY = 3;

    private static final String REST_CONTAINER_TYPE = "ecf.rest.client";

    private ConfigurationSerializerServiceTracker configurationSerializerServiceTracker;

    private BundleContext bundleContext;

    private Map<Method, String> resourcePaths = new HashMap<Method, String>();

    private Map<Class<?>, IRemoteResponseDeserializer> deserializers = new HashMap<Class<?>, IRemoteResponseDeserializer>();

    private Method getConfigurationSerializerMethod;

    private Method updateConfigurationMethod;

    public ConfigurationRemoteProxyEndpointServiceInvocationHandler() {

        try {

            getConfigurationSerializerMethod = ConfigurationService.class.getDeclaredMethod(
                "getConfigurationSerializer", String.class, String.class);

            updateConfigurationMethod = ConfigurationService.class.getDeclaredMethod("updateConfiguration",
                String.class, String.class, Object.class);

            Bundle bundle = FrameworkUtil.getBundle(this.getClass());

            bundleContext = bundle.getBundleContext();

            configurationSerializerServiceTracker = new ConfigurationSerializerServiceTracker(bundleContext);
            configurationSerializerServiceTracker.open();

            deserializers.put(ConfigurationType.class, new ConfigurationTypeDeserializer());

            resourcePaths.put(ConfigurationService.class.getDeclaredMethod("getConfigurationTypes", String.class),
                UriContainer.CONFIGURATION_TYPES_URI);

            resourcePaths.put(
                ConfigurationService.class.getDeclaredMethod("getConfigurationType", String.class, String.class),
                UriContainer.CONFIGURATION_TYPES_URI);
            resourcePaths.put(ConfigurationService.class.getDeclaredMethod("loadConfiguration", String.class,
                String.class, ConfigurationStatus.class), UriContainer.CONFIGURATION_INSTANCES_URI);
            resourcePaths.put(ConfigurationService.class.getDeclaredMethod("updateConfiguration", String.class,
                String.class, Object.class), UriContainer.CONFIGURATION_INSTANCES_URI);
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // this.remoteProxyService.setHost("localhost:8092");

        if (getConfigurationSerializerMethod.equals(method)) {
            return getConfigurationSerializer(args[0].toString(), args[1].toString());
        }

        String url = "http://localhost:8092";

        String resource = resourcePaths.get(method);

        if (args != null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(resource);
            for (int i = 0; i < args.length; i++) {
                Object param = args[i];

                if (null == param || !(param instanceof String)) {
                    continue;
                }
                stringBuilder.append(UriContainer.DELIMETER);
                stringBuilder.append(param);

            }

            resource = stringBuilder.toString();
        }

        if (updateConfigurationMethod.equals(method)) {
            return updateConfiguration(resource, args[0].toString(), args[1].toString(), args[2]);
        }

        Class<?> returnType = method.getReturnType();
        IRemoteResponseDeserializer deserializer = deserializers.get(returnType);

        // if the method returns collection, get its generic type instead
        if (Collection.class.isAssignableFrom(returnType)) {

            ParameterizedType type = (ParameterizedType) method.getGenericReturnType();
            Type[] actualTypeArguments = type.getActualTypeArguments();
            Type actualTypeArgument = actualTypeArguments[0];

            deserializer = deserializers.get(actualTypeArgument);

        }

        if (deserializer == null) {
            ConfiugrationInstanceDeserializer confDeserializer = new ConfiugrationInstanceDeserializer();
            ConfigurationSerializer configurationSerializer = getConfigurationSerializer(args[0].toString(),
                args[1].toString());
            confDeserializer.setConfigurationSerializer(configurationSerializer);
            deserializer = confDeserializer;

        }

        Request request = new Request(new HttpGetRequestType());

        IContainer container = ContainerFactory.getDefault().createContainer(REST_CONTAINER_TYPE, url);
        IRemoteServiceClientContainerAdapter adapter = (IRemoteServiceClientContainerAdapter) container
            .getAdapter(IRemoteServiceClientContainerAdapter.class);
        adapter.setConnectContextForAuthentication(ConnectContextFactory.createUsernamePasswordConnectContext(
            UriContainer.REMOTE_USERNAME, UriContainer.REMOTE_PASSWORD));
        adapter.setResponseDeserializer(deserializer);
        IRemoteCallParameter[] rcp = null;
        if (request.getRequestType() instanceof HttpPostRequestType) {
            rcp = RemoteCallParameterFactory.createParameters("", "");
        }
        IRemoteCallable callable = RestCallableFactory.createCallable(resource, resource, rcp,
            request.getRequestType(), IRestCall.DEFAULT_TIMEOUT);
        IRemoteServiceRegistration registration = adapter.registerCallables(new IRemoteCallable[] { callable }, null);
        IRemoteService restClientService = adapter.getRemoteService(registration.getReference());

        Object callSync = restClientService.callSync(RestCallFactory.createRestCall(resource, request.getBody()));

        // de-serializers only work with collections
        if (!returnType.isAssignableFrom(Collection.class) && Collection.class.isAssignableFrom(callSync.getClass())) {
            callSync = ((Collection) callSync).iterator().next();

        }

        return callSync;
    }

    public ConfigurationStatus updateConfiguration(String resource, String runtimeComponentId,
        String configurationTypeId, Object configuration) throws IOException, PlatformManagerException {
        HttpPostRequestType requestType = null;
        String url = "http://localhost:8092";
        if (configuration instanceof InputStream) {
            requestType = new HttpPostRequestType(INPUT_STREAM_REQUEST_ENTITY);
        } else if (configuration instanceof String) {
            requestType = new HttpPostRequestType(STRING_REQUEST_ENTITY);
        } else if (configuration instanceof File) {
            requestType = new HttpPostRequestType(FILE_REQUEST_ENTITY);
        } else if (configuration instanceof Properties) {
            ByteArrayOutputStream outputStream = null;
            requestType = new HttpPostRequestType(INPUT_STREAM_REQUEST_ENTITY);
            try {
                outputStream = new ByteArrayOutputStream();
                ((Properties) configuration).store(outputStream, null);
                configuration = new ByteArrayInputStream(outputStream.toByteArray());
            } finally {
                StreamUtil.close(outputStream);
            }
        }
        Request request = new Request(requestType, new Object[] { configuration });
        BaseRemoteResponseDeserializer<String> serializer = new BaseRemoteResponseDeserializer<String>() {
            @Override
            protected String createResponse(InputStream inputStream) {
                BufferedReader reader = null;
                try {
                    reader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) { // NOPMD
                        sb.append(line);
                        sb.append("\n");
                    }
                    return sb.toString();
                } catch (IOException e) {
                    ExceptionHandler.handle(e);
                } finally {
                    StreamUtil.close(reader);
                }

                return null;
            }

        };

        RemoteServiceProxy<Object> configurationInstanceProxy = new RemoteServiceProxy<Object>(serializer, url);
        String status = null;
        try {
            status = (String) configurationInstanceProxy.makeRestSyncCall(resource, request);
            return ConfigurationStatus.valueOf(status.trim());
        } catch (ECFException e) {
            ExceptionHandler.handle(e);
        }

        return null;
    }

    public ConfigurationSerializer getConfigurationSerializer(String runtimeComponentId, String configurationTypeId)
        throws PlatformManagerException {
        Map<String, ServiceComponent<ConfigurationSerializer>> serviceComponents = configurationSerializerServiceTracker
            .getServiceComponents();

        return ServiceComponentsUtil.findService(createUniqueId(runtimeComponentId, configurationTypeId),
            serviceComponents);
    }

    public static String createUniqueId(String componentId, String configurationTypeId) {
        StringBuilder confManagerUniqueId = new StringBuilder();
        if (null != componentId && null != configurationTypeId) {
            confManagerUniqueId.append(componentId);
            confManagerUniqueId.append(configurationTypeId);
            return confManagerUniqueId.toString();
        }

        throw new IllegalArgumentException(
            "ConfigurationManager should be registerd with runtimeComponentId and configurationTypeId properties");
    }
    
 

}
