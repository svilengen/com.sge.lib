/*
 * Copyright 2011 by Software AG
 *
 * Uhlandstrasse 12, D-64297 Darmstadt, GERMANY
 *
 * All rights reserved
 *
 * This software is the confidential and proprietary
 * information of Software AG ('Confidential Information').
 * You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license
 * agreement you entered into with Software AG or its distributors.
 */



import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.softwareag.platform.management.client.landscape.api.LandscapeManager;
import com.softwareag.platform.management.client.landscape.api.ManagedNode;
import com.softwareag.platform.management.client.landscape.impl.CacheManager;
import com.softwareag.platform.management.client.landscape.remote.proxy.LandscapeInventoryService;
import com.softwareag.platform.management.client.remote.services.InventoryServiceEndpointRemoteProxy;
import com.softwareag.platform.management.client.search.SearchCriteria;
import com.softwareag.platform.management.common.exceptions.PlatformManagerException;
import com.softwareag.platform.management.common.inventory.Product;
import com.softwareag.platform.management.common.inventory.RuntimeComponent;
import com.softwareag.platform.management.common.inventory.dto.ProductDTO;

public class LandscapeInventoryServiceImpl implements LandscapeInventoryService {

    // private final static WmJournalLogger logger = Logger.getLogger(LandscapeInventoryServiceImpl.class, "messages",
    // Constants.THIS_PRODUCT_ID, "LandscapeInventoryService");

    private static final int TIMEOUT = 30;

    private LandscapeManager landscapeManager;

    private ExecutorService remoteCallsExecutor = Executors.newFixedThreadPool(20);

    private CacheManager cacheManager = new CacheManager() {

        @Override
        public <V> void removeAll(Class<? extends V> type) {
            // TODO Auto-generated method stub

        }

        @Override
        public <K> void remove(K key) {
            // TODO Auto-generated method stub

        }

        @Override
        public <K, V> void put(K key, V value) {
            // TODO Auto-generated method stub

        }

        @Override
        public <V> Collection<V> getAll(Class<? extends V> type) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public <K, V> V get(K key, Class<? extends V> valueType) {
            // TODO Auto-generated method stub
            return null;
        }
    };

    private class SimpleCollectionWrapper<T> {
        private final Collection<T> actualCollection;

        SimpleCollectionWrapper(Collection<T> actualCollection) {
            this.actualCollection = actualCollection;

        }

        public Collection<T> getActualCollection() {
            return actualCollection;
        }

    }

    private class RetrieveProductsTask implements Callable<Collection<Product>> {

        private final ManagedNode managedNode;

        private final InventoryServiceEndpointRemoteProxy inventoryServiceEndpointRemoteProxy;

        private final CacheManager cacheManager;

        public RetrieveProductsTask(final ManagedNode managedNode,
            final InventoryServiceEndpointRemoteProxy inventoryServiceEndpointRemoteProxy, CacheManager cacheManager) {
            this.managedNode = managedNode;
            this.inventoryServiceEndpointRemoteProxy = inventoryServiceEndpointRemoteProxy;
            this.cacheManager = cacheManager;

        }

        @Override
        public Collection<Product> call() throws Exception {
            URL managedNodeUrl = managedNode.getUrl();

            // check cache first
            SimpleCollectionWrapper<Product> cachedProducts = (SimpleCollectionWrapper<Product>) this.cacheManager.get(
                managedNode, new SimpleCollectionWrapper<Product>(null).getClass());

            if (cachedProducts != null) {
                return cachedProducts.getActualCollection();
            }

            // no cache hit - do remote call
            Collection<Product> installedProducts = new ArrayList<Product>();
            try {
                installedProducts = inventoryServiceEndpointRemoteProxy.getInstalledProducts(managedNodeUrl);
            } catch (Exception e) {
                e.printStackTrace(System.err);
                ProductDTO errorProduct = createErrorProduct(e);
                installedProducts.add(errorProduct);
            }

            this.cacheManager.put(managedNode, new SimpleCollectionWrapper<Product>(installedProducts));

            return installedProducts;
        }

        private ProductDTO createErrorProduct(Exception e) {
            ProductDTO errorProduct = new ProductDTO();
            errorProduct.setDisplayName("Unable to retrieve Products of ManagedNode: " + managedNode.getUrl());
            errorProduct.setId(e.toString());
            return errorProduct;
        }

    }

    @Override
    public Collection<Product> getInstalledProducts(SearchCriteria searchCriteria) {

        Collection<Product> foundProducts = new ArrayList<Product>();

        if (searchCriteria == null) {

            Collection<ManagedNode> nodes = getMatchedNodes(searchCriteria);

            InventoryServiceEndpointRemoteProxy inventoryServiceSpmEndpointRemoteProxy = new InventoryServiceEndpointRemoteProxy();

            List<RetrieveProductsTask> retrieveProductTasks = new ArrayList<RetrieveProductsTask>();

            for (ManagedNode managedNode : nodes) {

                RetrieveProductsTask retrieveProductsTask = new RetrieveProductsTask(managedNode,
                    inventoryServiceSpmEndpointRemoteProxy, cacheManager);

                retrieveProductTasks.add(retrieveProductsTask);

            }

            List<Future<Collection<Product>>> retrieveProductTasksFutures = null;
            try {
                retrieveProductTasksFutures = remoteCallsExecutor.invokeAll(retrieveProductTasks, TIMEOUT,
                    TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace(System.err);
            }

            // invokeAll keeps the ordering aligned with that of the passed in tasks,
            // thus it is possible to associate Future with the Callable it represents in the following way
            Iterator<RetrieveProductsTask> retrieveProductTasksIterator = retrieveProductTasks.iterator();
            for (Future<Collection<Product>> retrieveProductTasksFuture : retrieveProductTasksFutures) {
                RetrieveProductsTask retrieveProductsTask = retrieveProductTasksIterator.next();
                try {
                    foundProducts.addAll(retrieveProductTasksFuture.get());
                } catch (ExecutionException e) {
                    // foundProducts.add(retrieveProductTasksFuture.getFailureQuote(e.getCause()));
                    e.printStackTrace(System.err);
                } catch (CancellationException e) {
                    // foundProducts.add(retrieveProductTasksFuture.getTimeoutQuote(e));
                    e.printStackTrace(System.err);
                } catch (InterruptedException e) {
                    e.printStackTrace(System.err);
                }
            }

            // boolean completedAllNodes = waitForNodes(allNodesQueriedFlag);
            // handleWaitForNodes(completedAllNodes);
        }

        return foundProducts;
    }

    private Collection<ManagedNode> getMatchedNodes(SearchCriteria searchCriteria) {
        Collection<ManagedNode> nodes = new ArrayList<ManagedNode>();
        if (searchCriteria == null) {
            nodes = landscapeManager.getNodes();
        }

        return nodes;
    }

    @Override
    public Collection<RuntimeComponent> getRuntimeComponents(SearchCriteria searchQuery) {
        final Collection<RuntimeComponent> allRuntimeComponents = Collections
            .synchronizedCollection(new ArrayList<RuntimeComponent>());

        if (searchQuery == null) {

            Collection<ManagedNode> nodes = getMatchedNodes(searchQuery);

            final CountDownLatch allNodesQueriedFlag = getLatchForNodes(nodes);

            final InventoryServiceEndpointRemoteProxy inventoryServiceSpmEndpointRemoteProxy = new InventoryServiceEndpointRemoteProxy();

            for (ManagedNode managedNode : nodes) {
                final URL url = managedNode.getUrl();

                remoteCallsExecutor.submit(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Collection<RuntimeComponent> runtimeComponents = inventoryServiceSpmEndpointRemoteProxy
                                .getRuntimeComponents(url);
                            allRuntimeComponents.addAll(runtimeComponents);
                            allNodesQueriedFlag.countDown();
                        } catch (PlatformManagerException e) {
                            // logger.errorString(e.toString());
                            // FIXME
                            e.printStackTrace(System.err);
                        }

                    }
                });

            }

            boolean completedAllNodes = waitForNodes(allNodesQueriedFlag);
            handleWaitForNodes(completedAllNodes);
        }

        return allRuntimeComponents;
    }

    private CountDownLatch getLatchForNodes(Collection<ManagedNode> nodes) {
        final CountDownLatch allNodesQueriedFlag = new CountDownLatch(nodes.size());
        return allNodesQueriedFlag;
    }

    /**
     * Throws RuntimeException wrapping InterruptedExcepton
     * @param allNodesQueriedFlag
     * @return
     */
    private boolean waitForNodes(final CountDownLatch allNodesQueriedFlag) {
        try {
            // wait for specific amount of time for remote calls to all found nodes to complete
            return allNodesQueriedFlag.await(TIMEOUT, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException(
                "Current Thread has been interrupted while waiting for remote calls to all nodes to complete.", e);
        }
    }

    private void handleWaitForNodes(boolean completedAllNodes) {
        if (!completedAllNodes) {
            throw new RuntimeException("Response from nodes took longer than " + TIMEOUT + TimeUnit.SECONDS + ".");
        }
    }

    public void unsetLandscapeManager(LandscapeManager landscapeManager) {
        this.landscapeManager = null;
    }

    public void setLandscapeManager(LandscapeManager landscapeManager) {
        this.landscapeManager = landscapeManager;
    }

}
