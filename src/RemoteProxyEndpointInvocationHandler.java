/*
 * Copyright 2011 by Software AG
 *
 * Uhlandstrasse 12, D-64297 Darmstadt, GERMANY
 *
 * All rights reserved
 *
 * This software is the confidential and proprietary
 * information of Software AG ('Confidential Information').
 * You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license
 * agreement you entered into with Software AG or its distributors.
 */



import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.ecf.core.ContainerFactory;
import org.eclipse.ecf.core.IContainer;
import org.eclipse.ecf.core.security.ConnectContextFactory;
import org.eclipse.ecf.remoteservice.IRemoteService;
import org.eclipse.ecf.remoteservice.IRemoteServiceRegistration;
import org.eclipse.ecf.remoteservice.client.IRemoteCallParameter;
import org.eclipse.ecf.remoteservice.client.IRemoteCallable;
import org.eclipse.ecf.remoteservice.client.IRemoteResponseDeserializer;
import org.eclipse.ecf.remoteservice.client.IRemoteServiceClientContainerAdapter;
import org.eclipse.ecf.remoteservice.client.RemoteCallParameterFactory;
import org.eclipse.ecf.remoteservice.rest.IRestCall;
import org.eclipse.ecf.remoteservice.rest.RestCallFactory;
import org.eclipse.ecf.remoteservice.rest.RestCallableFactory;
import org.eclipse.ecf.remoteservice.rest.client.HttpGetRequestType;
import org.eclipse.ecf.remoteservice.rest.client.HttpPostRequestType;

import com.softwareag.platform.management.client.remote.proxy.Request;
import com.softwareag.platform.management.client.remote.proxy.UriContainer;
import com.softwareag.platform.management.client.remote.proxy.configuration.ConfigurationTypeDeserializer;
import com.softwareag.platform.management.client.remote.proxy.inventory.ProductDeserializer;
import com.softwareag.platform.management.client.remote.proxy.inventory.RuntimeComponentsDeserializer;
import com.softwareag.platform.management.client.remote.proxy.jobmanager.JobManagerDeserializer;
import com.softwareag.platform.management.common.configuration.ConfigurationType;
import com.softwareag.platform.management.common.inventory.InventoryService;
import com.softwareag.platform.management.common.inventory.Product;
import com.softwareag.platform.management.common.inventory.RuntimeComponent;
import com.softwareag.platform.management.common.jobs.Job;
import com.softwareag.platform.management.common.lifecycle.LifecycleOperationsService;
import com.softwareag.platform.management.common.lifecycle.OperationInfo;

class RemoteProxyEndpointInvocationHandler implements InvocationHandler {

    private static final String REST_CONTAINER_TYPE = "ecf.rest.client";

    private Map<Class<?>, IRemoteResponseDeserializer> deserializers = new HashMap<Class<?>, IRemoteResponseDeserializer>();

    private Map<Method, String> resourcePaths = new HashMap<Method, String>();

    public RemoteProxyEndpointInvocationHandler() {

        deserializers.put(Product.class, new ProductDeserializer());
        deserializers.put(RuntimeComponent.class, new RuntimeComponentsDeserializer());
        deserializers.put(ConfigurationType.class, new ConfigurationTypeDeserializer());
        deserializers.put(Job.class, new JobManagerDeserializer());

        try {
            resourcePaths.put(InventoryService.class.getDeclaredMethod("getInstalledProducts"),
                UriContainer.PRODUCTS_URI);
            resourcePaths.put(InventoryService.class.getDeclaredMethod("getInstalledProduct", String.class),
                UriContainer.PRODUCTS_URI);
            resourcePaths.put(InventoryService.class.getDeclaredMethod("getRuntimeComponents"),
                UriContainer.RUNTIME_COMPONENTS_URI);
            resourcePaths.put(InventoryService.class.getDeclaredMethod("getRuntimeComponent", String.class),
                UriContainer.RUNTIME_COMPONENTS_URI);

            resourcePaths.put(LifecycleOperationsService.class.getDeclaredMethod("start", String.class,
                OperationInfo.class, String.class), UriContainer.LIFECYCLE_OPERATIONS_URI);
            resourcePaths.put(LifecycleOperationsService.class.getDeclaredMethod("stop", String.class,
                OperationInfo.class, String.class), UriContainer.LIFECYCLE_OPERATIONS_URI);
            resourcePaths.put(LifecycleOperationsService.class.getDeclaredMethod("startInDebugMode", String.class,
                OperationInfo.class, String.class), UriContainer.LIFECYCLE_OPERATIONS_URI);
            resourcePaths.put(LifecycleOperationsService.class.getDeclaredMethod("restart", String.class,
                OperationInfo.class, String.class), UriContainer.LIFECYCLE_OPERATIONS_URI);

        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // this.remoteProxyService.setHost("localhost:8092");

        String url = "http://localhost:8092";

        String resource = resourcePaths.get(method);

        if (args != null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(resource);
            for (int i = 0; i < args.length; i++) {
                Object param = args[i];

                if (null == param || !(param instanceof String)) {
                    continue;
                }
                stringBuilder.append(UriContainer.DELIMETER);
                stringBuilder.append(param);

            }

            resource = stringBuilder.toString();
        }

        Class<?> returnType = method.getReturnType();
        IRemoteResponseDeserializer deserializer = deserializers.get(returnType);

        // if the method returns collection, get its generic type instead
        if (Collection.class.isAssignableFrom(returnType)) {

            ParameterizedType type = (ParameterizedType) method.getGenericReturnType();
            Type[] actualTypeArguments = type.getActualTypeArguments();
            Type actualTypeArgument = actualTypeArguments[0];

            deserializer = deserializers.get(actualTypeArgument);

        }

        Request request = new Request(new HttpGetRequestType());

        IContainer container = ContainerFactory.getDefault().createContainer(REST_CONTAINER_TYPE, url);
        IRemoteServiceClientContainerAdapter adapter = (IRemoteServiceClientContainerAdapter) container
            .getAdapter(IRemoteServiceClientContainerAdapter.class);
        adapter.setConnectContextForAuthentication(ConnectContextFactory.createUsernamePasswordConnectContext(
            UriContainer.REMOTE_USERNAME, UriContainer.REMOTE_PASSWORD));
        adapter.setResponseDeserializer(deserializer);
        IRemoteCallParameter[] rcp = null;
        if (request.getRequestType() instanceof HttpPostRequestType) {
            rcp = RemoteCallParameterFactory.createParameters("", "");
        }
        IRemoteCallable callable = RestCallableFactory.createCallable(resource, resource, rcp,
            request.getRequestType(), IRestCall.DEFAULT_TIMEOUT);
        IRemoteServiceRegistration registration = adapter.registerCallables(new IRemoteCallable[] { callable }, null);
        IRemoteService restClientService = adapter.getRemoteService(registration.getReference());

        Object callSync = restClientService.callSync(RestCallFactory.createRestCall(resource, request.getBody()));

        // de-serializers only work with collections
        if (!returnType.isAssignableFrom(Collection.class) && Collection.class.isAssignableFrom(callSync.getClass())) {
            callSync = ((Collection) callSync).iterator().next();

        }

        return callSync;
    }

}