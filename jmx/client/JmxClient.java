package client;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.rmi.ssl.SslRMIClientSocketFactory;

public class JmxClient {

	private static String hostname = "localhost";
	private static int portNum = 5002;

	/**
	 * @param args
	 * @throws IOException
	 * @throws NullPointerException
	 * @throws MalformedObjectNameException
	 * @throws InstanceNotFoundException 
	 */
	public static void main(String[] args) throws IOException,
			MalformedObjectNameException, NullPointerException, InstanceNotFoundException {
		JMXServiceURL url = new JMXServiceURL("service:jmx:rmi://" + hostname
				+ ":" + portNum + "/jndi/rmi://" + hostname + ":" + portNum
				+ "/jmxrmi");
		
		//service:jmx:rmi://pcbgsge:8094/jndi/rmi://pcbgsge:8094/jmxrmi

		Map env = new HashMap();
		boolean authenticationEnabled = "true".equals(System
				.getProperty("com.sun.management.jmxremote.authenticate"));

		if (authenticationEnabled) {
			String[] creds = { "jmxuser", "manage" };
			env.put(JMXConnector.CREDENTIALS, creds);
		}

		boolean sslEnabled = "true".equals(System
				.getProperty("com.sun.management.jmxremote.ssl"));

		if (sslEnabled) {
			env.put("com.sun.jndi.rmi.factory.socket",
					new SslRMIClientSocketFactory());
		}

		JMXConnector connector = JMXConnectorFactory.connect(url, env);

		MBeanServerConnection mServerConnection = connector
				.getMBeanServerConnection();
		ObjectName mThreadObjectName = ObjectName
				.getInstance("java.lang:type=Threading"); //$NON-NLS-1$
		
		mServerConnection.getObjectInstance(mThreadObjectName);

		System.out.println(mThreadObjectName);

	}

}
