package server;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.rmi.registry.LocateRegistry;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.management.MBeanServer;
import javax.management.remote.JMXAuthenticator;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXPrincipal;
import javax.management.remote.JMXServiceURL;
import javax.management.remote.rmi.RMIConnectorServer;
import javax.rmi.ssl.SslRMIClientSocketFactory;
import javax.rmi.ssl.SslRMIServerSocketFactory;
import javax.security.auth.Subject;

public class JmxServer {

	public static final String LISTENER_NAME_JMX = "jmx";

	public static final String PROP_JMX_PORT = "portal." + LISTENER_NAME_JMX
			+ "." + "port";

	private static final String MWS_PRIVILEGE_JMX_CONNECT = "mws.privilege.jmx.connect";

	private static ScheduledExecutorService singleThreadScheduledExecutor = Executors
			.newSingleThreadScheduledExecutor();
	private static JMXConnectorServer cs;

	private static void startBackgroundThread() {
		singleThreadScheduledExecutor.scheduleAtFixedRate(new Runnable() {

			public void run() {
				// System.out.println("Executed at " + System.nanoTime());

			}

		}, 0, 5, TimeUnit.SECONDS);

	}

	/**
	 * MWS specific implementation of {@link JMXAuthenticator}
	 * 
	 * @author bgsge
	 * 
	 */
	private static class MwsJmxAuthenticator implements JMXAuthenticator {

		public Subject authenticate(Object credentials) {

			// Verify that credentials is of type String[].
			//
			if (!(credentials instanceof String[])) {
				// Special case for null so we get a more informative message
				if (credentials == null) {
					throw new SecurityException("Credentials required");
				}
				throw new SecurityException("Credentials should be String[]");
			}

			// Verify that the array contains two elements
			// (username/password) (there could be more e.g. realm).
			//
			final String[] aCredentials = (String[]) credentials;
			if (aCredentials.length != 2) {
				throw new SecurityException(
						"Credentials should have 2 elements");
			}

			// Perform MWS specific authentication and authorization
			//
			String username = (String) aCredentials[0];
			String password = (String) aCredentials[1];

			try {
				authenticate(username, password);
			} catch (Exception e) {
				throw new SecurityException("JMX Authentication failed!", e);
			}

			// Everything went ok,
			// so return newly created JAAS Subject
			// holding JMX principal for the given credentials.
			//
			return new Subject(true, Collections.singleton(new JMXPrincipal(
					username)), Collections.EMPTY_SET, Collections.EMPTY_SET);

		}

		/**
		 * Performs MWS specific authentication.
		 * 
		 * @param context
		 *            For accessing MWS APIs.
		 * @param username
		 * @param password
		 * @throws PortalException
		 *             If something goes wrong while making use of MWS
		 *             authentication facilities.
		 * @throws BizException
		 *             If authentication fails.
		 */
		private void authenticate(String username, String password) {
			if ("jmxuser".equals(username) && "manage".equals(password)) {
				System.out.println("JMX Authentication successfull!");

			} else {
				throw new SecurityException();
			}

		}
	}

	/**
	 * Start JMX Connector Server configured by using JVM system properties.
	 * 
	 * @throws IOException
	 *             If something goes wrong while the server is starting.
	 * @throws IllegalArgumentException
	 *             If misconfigured.
	 */
	private static void startJMX() throws IOException {
		// System.getProperty("com.sun.management.jmxremote.port");
		// Do not use standard JMX property
		String portNum = System.getProperty(PROP_JMX_PORT);
		if (portNum == null) {
			portNum = System.getenv(PROP_JMX_PORT);
		}

		int port;
		try {
			port = Integer.valueOf(portNum);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException(
					"Port number cannot be parsed. Port parameter value = "
							+ portNum, e);
		}

		final String hostname = InetAddress.getLocalHost().getHostName();

		// Environment map, configuration for JMX Connector Server
		//
		System.out.println("Initialize the environment map");
		Map env = new HashMap();		

		boolean authenticationEnabled = "true".equals(System
				.getProperty("com.sun.management.jmxremote.authenticate"));

		if (authenticationEnabled) {
			env.put(JMXConnectorServer.AUTHENTICATOR, new MwsJmxAuthenticator());

		}

		boolean sslEnabled = "true".equals(System
				.getProperty("com.sun.management.jmxremote.ssl"));

		if (sslEnabled) {
			final SslRMIClientSocketFactory csf = new SslRMIClientSocketFactory();
			final SslRMIServerSocketFactory ssf = new SslRMIServerSocketFactory();
			// Now specify the SSL Socket Factories:
			//
			// For the client side (remote)
			//
			env.put(RMIConnectorServer.RMI_CLIENT_SOCKET_FACTORY_ATTRIBUTE, csf);

			// For the server side (local)
			//
			env.put(RMIConnectorServer.RMI_SERVER_SOCKET_FACTORY_ATTRIBUTE, ssf);

			// Create local RMI registry listening on the specified port
			//
			LocateRegistry.createRegistry(port, csf, ssf);
			
			// For binding the JMX RMI Connector Server with the registry
			// created above:
			//
			env.put("com.sun.jndi.rmi.factory.socket", csf);

		} else {
			LocateRegistry.createRegistry(port);
		}

		// Construct JMX Connector Server URL for the specific host and port
		//
		System.out.println("Initialize the JMX Service URL");
		JMXServiceURL url = new JMXServiceURL("service:jmx:rmi://" + hostname
				+ ":" + portNum + "/jndi/rmi://" + hostname + ":" + portNum
				+ "/jmxrmi");

		// Instantiate the platform MBean server
		//
		System.out.println("Create the MBean server");
		// MBeanServer mbs = MBeanServerFactory.createMBeanServer();
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();

		// Start JMX Connector Server
		//
		System.out.println("Start the JMX Connector Server at " + url);
		cs = JMXConnectorServerFactory.newJMXConnectorServer(url, env, mbs);
		cs.start();

	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		try {
			startJMX();
			startBackgroundThread();
		} catch (Exception e) {
			singleThreadScheduledExecutor.shutdownNow();
			if (cs != null) {
				cs.stop();
			}
			e.printStackTrace();

		}

	}

}
